# run: python ePaper_trafficLight_Test.py:
# Script loopt through an MQTT message and send signals to:
# e-Paper
# TrafficLight
# Buzzer

import trafficLib
import ePaperLib
from time import sleep

supportedCommands  = ["RED", "ORANGE", "GREEN", "BEEP", "MESSAGE", "WAIT"]

def mqtt_Process(msg):
    #chop up commands
    commandList = msg.split(";")
    #run every command
    for command in commandList:
        #Determine action and value.
        actionList = command.split("-")
        action = actionList[0]
        value = actionList[1]
        #Check if action is supported:
        if (action.upper() in supportedCommands):
            if (action=="message"):
                print("sending message to e-Paper: " + action)
                ePaperLib.mqtt_ePaper(value)
            else:
                trafficLib.processTrafficMsg(action, value)
        else:
            print("command: " + command + " is unsupported.")

mqttMsg = "message-Oelan IT presents Techday;red-0;orange-0;green-0;wait-2000;message-Night rider;red-4000;orange-2000;red-0;green-1;message-Go FasTest!;wait-500;beep-40;wait-10000;green-0"

mqtt_Process(mqttMsg)
sleep(5)
ePaperLib.clear_ePaper()