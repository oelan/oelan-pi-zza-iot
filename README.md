# Oelan PI-ZZA - IoT - Let's setup Raspberry Pi

## Beschrijving

Deze middag staat in het teken van de setup van de Raspberry pi.  
We gaan de pi configureren, scripts runnen en eventuele fouten herstellen.  
Het doel is dat aan het einde van de PI-ZZA sessie iedereen het stoplicht, buzzer en e-paper aan de praat heeft en dat we elkaar berichten kunnen sturen.

![e](./pic/setup.jpg)

## Hardware connecties

![1](./pic/pi4b-1.jpg)

![2](./pic/pi4b-2.jpg)

### e-Paper pinout

|Waveshare connector|Kleur Draad|RPi Pin|GPIO|
|---|---|---|---|
|VCC|Grijs|1|3V3|
|GND|Bruin|9|GND|
|DIN|Blauw|19|GPIO 10 (MOSI)|
|CLK|Geel|23|GPIO 11 (SCLK)|
|CS|Oranje|24|GPIO 8 (CE0)|
|DC|Groen|22|GPIO 25|
|RST|Wit|11|GPIO 17|
|BUSY|Paars|18|GPIO 24|

### Traffic Light pinout

|Pin|RPi Pin|GPIO|
|---|---|---|
|Rood|40|GPIO 21|
|Oranje|38|GPIO 20|
|Groen|36|GPIO 16|
|GND|34|GND|

### Buzzer KY-012 pinout

|Buzzer Pin|Kleur Draad|RPi Pin|GPIO|
|---|---|---|---|
|Signal|Geel|8|GPIO 14|
|GND|Zwart|6|GND|
|+V|Rood|Rood|4|5V|

## install e-paper Libraries

**[Source on waveshare -> Hardware/Software setup](https://www.waveshare.com/wiki/2.9inch_e-Paper_HAT_(D))**

----------------

Enable SPI interface

```shell
sudo raspi-config
```

- Choose Interfacing Options
- SPI
- Yes to enable SPI interface

```shell
sudo reboot
```

----------------
Install BCM2835 libraries

```shell
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.60.tar.gz
tar zxvf bcm2835-1.60.tar.gz
cd bcm2835-1.60/
sudo ./configure
sudo make
sudo make check
sudo make install
```

----------------

Install wiringPi libraries

```shell
sudo apt-get install wiringpi
```

----------------

Update Pi4

```shell
cd /tmp
wget https://project-downloads.drogon.net/wiringpi-latest.deb
sudo dpkg -i wiringpi-latest.deb
gpio -v 
```

----------------
Install Python2 libraries

```shell
sudo apt-get update
sudo apt-get install python-pip
sudo apt-get install python-pil
sudo apt-get install python-numpy
sudo pip install RPi.GPIO
sudo pip install spidev
```

----------------

Install Python3 libraries

```shell
sudo apt-get update
sudo apt-get install python3-pip
sudo apt-get install python3-pil
sudo apt-get install python3-numpy
sudo pip3 install RPi.GPIO
sudo pip3 install spidev
```

----------------

Install paho-mqtt library

```shell
pip install paho-mqtt
```

----------------

Install git and checkout project

```shell
sudo apt-get install git -y
cd /home/pi/
git clone https://bitbucket.org/oelan/oelan-pi-zza-iot.git
cd oelan-pi-zza-iot/examples
```

## Example scripts

### Voordat je de examples uitvoert

Er zijn twee types ePapers uitgeleverd: Afhankelijk van de versie, is een instelling vereist in "configLib.py".

- V1: Te herkennen aan een ronde "QC" sticker op de voorkant (witte zijde) van het ePaper. 
- V2: Te herkennen aan een ronder "V2" sticker op de voorkant (witte) zijde) van de ePaper.

Kies __*display_version = 1*__ voor het V1 scherm, en __*display_version = 2*__ voor het V2 scherm.

 
|Script|Description|Command|
|---|---|---|
|configLib.py|generic library file with mqtt parameters|--|
|ePaperLib.py|lib for sending mqtt messages to e-paper|--|
|trafficLib.py|lib for processing traffic light and buzzer|-- |
|ePaperTest.py|Test: initialize e-paper with Oelan logo and sends message to e-paper |python ePaperTest.py|
|ePaper_trafficLight_Test.py|Test: process "mqtt message action string". All three devices are triggered.|python ePaper_trafficLight_Test.py|
|mqttReceiveSingle.py|Receives a single (retained) message from mqtt queue.| python mqttReceiveSingle.py|
|mqttSendSingle.py|Sends a single message to the mqtt queue. Argument optional|python mqttSendSingle.py "message-Greetz van Matthijs en Juul;red-1;orange-1;green-1;wait-2000;beep-40" |
| | |python mqttSendSingle.py "message-Sending Out;beep-80;wait-80;beep-80;wait-80;beep-80;wait-240;beep-240;wait-80;beep-240;wait-80;beep-240;wait-80;beep-80;wait-80;beep-80;wait-80;beep-80;message-An SOS"|
|mqttReceiveLoop.py|Continuous loop which processes "mqtt message action string"  |python mqttReceiveLoop.py|

## Using Visual Studio with SSH connection

[Vanuit Visual studio kun je direct op de pi bestanden editen en runnen!! :-)](https://www.hanselman.com/blog/VisualStudioCodeRemoteDevelopmentOverSSHToARaspberryPiIsButter.aspx)

## Enable "listener" at startup

Editing rc.local

```shell 
sudo nano /etc/rc.local
```

Add lines these line, before "exit 0"

```shell
export PYTHONPATH="/home/pi/.local/lib/python2.7/site-packages"
sleep 10 && python /home/pi/oelan-pi-zza-iot/examples/mqttReceiveLoop.py &
```
**Don't forget the & or you are in trouble !!**

Then do a reboot 
```shell
sudo reboot
```
Check what happened during startup with:
  
```shell
tail -100 /var/log/syslog
``` 
or 
```shell 
sudo nano /var/log/syslog
```

To continuously "tail" the log:

```shell
tail -f /var/log/syslog
```
