#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#--------------------
#ePaper Display config
#--------------------
#display_version = 1 #for ePaper with "QC" sticker
display_version = 2  #for ePaper with "V2" sticker

#--------------------
#MQTT config
#--------------------
broker_address  = "mqtt.test-cloud.nl"
#port=1883 #no ssl
port=8883  #with ssl
timeout=10 # in seconds
username="oelan" #username for the mqtt broker.
password="03l4n" #password for the mqtt broker.

topic="oelan-it" #mqtt topic to send messages to.
