# sends a message to the ePaper device.
# run ePaperTest.py:

import trafficLib
import ePaperLib
from time import sleep

value = "Vroeger was ik een twijfelaar, ik ben daar nu niet meer zo zeker van."

ePaperLib.init_ePaper()
sleep(3)
ePaperLib.mqtt_ePaper(value)
sleep(5)
ePaperLib.clear_ePaper()

