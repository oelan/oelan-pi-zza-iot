#First lets begin by importing two libraries that we require.
#We require the RPi.GPIO Library to access the GPIO Pins
#And we require sleep from time to be able to add a delay.
import RPi.GPIO as GPIO  #This line imports RPi.GPIO but sets it to IO meaning we can type in IO on every line instead of RPi.GPIO.
from time import sleep #This imports only the sleep function from the time library.

GPIO.setmode(GPIO.BCM) #This sets the RPi.GPIO Library to use the BCM/GPIO Numbering scheme which is used on the TrafficHAT PCB.

#Now we want to make a variable for each pin used.
RED = 21
ORANGE = 20
GREEN = 16
BEEP = 14

#Next we need to configure these pins
#The LEDs & BEEP are all Outputs, this means we need to configure these all to be an output.
#---------------------------------------
#To do this we are going to add them all to an Array and iterate through the Array to set each one up
outputs     = [RED, ORANGE, GREEN, BEEP]
outputsStr  = ["RED", "ORANGE", "GREEN", "BEEP"]

GPIO.setwarnings(False) # to disable warnings.
#Next we need to iterate through each one using an For loop
for output in outputs:
    #This will then pass the number of the output to the variable output
    GPIO.setup(output,GPIO.OUT)

#set pin as output and set signal for x milliseconds.
def processTrafficMsg(action, duration):
    #if duration is not a number, set to default value
    try:
        duration = int(duration)
    except ValueError:
        duration = 2000

    print("action: " + action + ", duration: " + str(duration) + " [ms]")
    #one of the outputs, also BEEP.
    if (action != "wait"):
        pin = outputs[outputsStr.index(action.upper())]

    #Switch on
    if (duration == 1 and action != "wait"):
        GPIO.output(pin,1)
    #Switch off
    if (duration == 0 and action != "wait"):
        GPIO.output(pin,0)
    #On-Off
    if (duration > 1 and action != "wait"):
        GPIO.output(pin,1)
        sleep(duration/1000.0) #sleep in ms
        GPIO.output(pin,0)
    if (action == "wait"):
        sleep(duration/1000.0) #sleep in ms
