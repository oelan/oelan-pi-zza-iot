#receives single MQTT message from queue.
#run: python mqttReceiveSngle.py
import paho.mqtt.client as mqtt
import ssl
import sys
import time
#Part of oelan project:
import configLib

#optional input
if len(sys.argv) > 1:
   msg = sys.argv[1]
else:
   msg = 'Groeten van Oelan.'

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(configLib.topic)

def on_message(client, userdata, msg):
    receivedMessage = str(msg.payload.decode("utf-8"))
    print(msg.topic+": " + receivedMessage)
    print("message qos=", msg.qos)
    print("message retain flag=", msg.retain)
    client.disconnect()

#setup mqtt client
client = mqtt.Client()
client.tls_set(ca_certs=None, certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLS, ciphers=None)
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(configLib.username, configLib.password)
client.connect(configLib.broker_address,configLib.port)

#Loop until on_message, than disconnect.
client.loop_forever()
