# run mqttclient.py:
# oelan-techday-iot\python\mqtt> python.exe .\lib\mqttclient.py
#
# PREPARE FIRST:
# added to %path%: #<python install path>\Python\Python38\Scripts
# added system variable: #PYTHONPATH = <path>\bitbucket\oelan-techday-iot\python\mqtt
# Run in terminal: #pip install paho-mqtt

# http://www.steves-internet-guide.com/into-mqtt-python-client/
import paho.mqtt.client as mqtt
import ssl
#Part of oelan project:
import configLib
import ePaperLib
import trafficLib

supportedCommands  = ["RED", "ORANGE", "GREEN", "BEEP", "MESSAGE", "WAIT"]
#Example message.
mqttMsg = "message-Oelan IT presents Techday;red-0;orange-0;green-0;wait-2000;message-Night rider;red-4000;orange-2000;red-0;green-1;message-Go FasTest!;wait-500;beep-40;wait-10000;green-0"

################################################################################
# The callback for when the client receives a CONNACK response from the server.

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(configLib.topic)

# The callback for when a PUBLISH message is received from the server.

def on_message(client, userdata, msg):
    receivedMessage = str(msg.payload.decode("utf-8"))
    print(msg.topic+": " + receivedMessage)
    print("message qos=", msg.qos)
    print("message retain flag=", msg.retain)
    #chop up commands
    commandList = receivedMessage.split(";")
    #run every command
    for command in commandList:
        #Determine action and value
        actionList = command.split("-")
        #if there is no "-" sign, action = "message"
        if (len(actionList)==1):
            action = "message"
            value = actionList[0]
        else:
            action = actionList[0]
            value = actionList[1]
        #Check if action is supported:
        if (action.upper() in supportedCommands):
            if (action=="message"):
                print("sending message to e-Paper: " + action)
                ePaperLib.mqtt_ePaper(value)
            else:
                trafficLib.processTrafficMsg(action, value)
        else:
            print("command: " + command + " is unsupported.")

################################################################################

#init e-paper screen
ePaperLib.init_ePaper()

#setup mqtt client
client = mqtt.Client()
client.tls_set(ca_certs=None, certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLS, ciphers=None)
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(configLib.username, configLib.password)
client.connect(configLib.broker_address,configLib.port, keepalive=60, bind_address="")

# This loop receives mqtt messages
client.loop_forever()
