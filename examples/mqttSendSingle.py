#sends single MQTT message to queue.
#run: python mqttSendSingle.py "mesage to send to MQTT queue..." (optional parameter)
import paho.mqtt.client as mqtt
import ssl
import sys
#Part of oelan project:
import configLib

#optional input
if len(sys.argv) > 1:
   msg = sys.argv[1]
else:
   msg = "message-Oelan IT presents Techday;red-0;orange-0;green-0;wait-2000;message-Night rider;red-4000;orange-2000;red-0;green-1;message-Go FasTest!;wait-500;beep-40;wait-10000;green-0""message-Oelan IT presents Techday;red-0;orange-0;green-0;wait-2000;message-Night rider;red-4000;orange-2000;red-0;green-1;message-Go FasTest!;wait-500;beep-40;wait-10000;green-0"

#setup mqtt client
client = mqtt.Client()
client.tls_set(ca_certs=None, certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLS, ciphers=None)
client.username_pw_set(configLib.username, configLib.password)
client.connect(configLib.broker_address,configLib.port)
client.publish(configLib.topic, msg, retain=True)
