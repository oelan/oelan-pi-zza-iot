#sends single MQTT message to queue.
#go to dir: cd ./oelan-pi-zza-iot/examples 
#run: python mqttSendSingleMemory.py "Lucky Luke" "A1" "B1"

import paho.mqtt.client as mqtt
import ssl
import sys
#Part of oelan project:
import configLib
import json

alias = sys.argv[1]
img1  = sys.argv[2]
img2  = sys.argv[3]

msg = {
   "alias": alias,
   "img1":  img1,
   "img2":  img2
}

#setup mqtt client
client = mqtt.Client()
client.tls_set(ca_certs=None, certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLS, ciphers=None)
client.username_pw_set(configLib.username, configLib.password)
client.connect(configLib.broker_address,configLib.port)
print("memory message: ", json.dumps(msg))
client.publish("memory", json.dumps(msg), retain=True)
