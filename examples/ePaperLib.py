#!/usr/bin/python
# -*- coding:utf-8 -*-
import sys
import os
picdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'pic')
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)

import logging
import configLib
from waveshare_epd import epd2in9
from waveshare_epd import epd2in9_V2
import time
from PIL import Image,ImageDraw,ImageFont
import traceback

logging.basicConfig(level=logging.DEBUG)
epd = epd2in9.EPD()

def init_and_clearV1():
        epd = epd2in9.EPD()
        logging.info("init and Clear V1")
        epd.init(epd.lut_full_update)
        epd.Clear(0xFF)
       

def init_and_clearV2():
        epd = epd2in9_V2.EPD()
        logging.info("init and Clear V2")
        epd.init()
        epd.Clear(0xFF)


def mqtt_ePaper(mqttMsg):
    init_and_clearV1() if configLib.display_version == 1  else init_and_clearV2()

    # partial update
    font24 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)
    font18 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 18)
    
    time_image = Image.new('1', (epd.height, epd.width), 255)
    time_draw = ImageDraw.Draw(time_image)

    time_draw.rectangle((10, 10, 120, 50), fill = 255)
    time_draw.text((10, 5), "MQTT message:", font = font24, fill = 0)
    time_draw.text((10, 35), mqttMsg[0:28] + " ", font = font18, fill = 0) #extra space improves readability
    time_draw.text((10, 65), mqttMsg[28:56] + " ", font = font18, fill = 0)
    time_draw.text((10, 95), mqttMsg[56:84] + " ", font = font18, fill = 0)
    newimage = time_image.crop([10, 10, 120, 50])
    time_image.paste(newimage, (10,10))  
    epd.display(epd.getbuffer(time_image))

def init_ePaper():
    logging.info("epd2in9 Demo")
    init_and_clearV1() if configLib.display_version == 1  else init_and_clearV2()
    logging.info("read init file on window")
    Himage2 = Image.new('1', (epd.height, epd.width), 255)  # 255: clear the frame
    bmp = Image.open(os.path.join(picdir, 'init.bmp'))
    Himage2.paste(bmp, (0,0))
    epd.display(epd.getbuffer(Himage2))
    #sleep and exit
    epd.sleep()

def clear_ePaper():
    init_and_clearV1() if configLib.display_version == 1  else init_and_clearV2()
    logging.info("ePaper cleared, going to sleep.")
    epd.sleep()